# BimLean

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.1.

## Development (Client-side only rendering)

`npm start` which will run ng serve.

## Development (Server-side rendering)

Run `npm run dev:ssr` for starting server and frontend. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Production

Run `npm run build:ssr && npm run serve:ssr` Compiles your application and spins up a Nest server to serve your Universal application on `http://localhost:4200/`. 

eth
eth2020

